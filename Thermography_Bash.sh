#!/bin/bash

# set input parameters
frames=1800 # set video duration as number of frames to capture.
folder='tiffs' # set name of folder

# perform FFC event
echo -e -n '\x8E\x00\x12\xC0\xFF\xEE\x00\x05\x00\x07\xFF\xFF\xFF\xFF\x6C\x5E\xAE' > /dev/ttyACM0


sleep 10


./BosonUSB/BosonUSB f$folder t$frames
