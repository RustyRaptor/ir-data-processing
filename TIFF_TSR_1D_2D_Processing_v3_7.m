
function TIFF_TSR_1D_2D_Processing_v3_7(filename,existing_logfile,heat_event_frame)
%% Program to process thermography TIFF sequence files
%% G. Ray Ely, Sandia National Labs, Org. 06621, grely@sandia.gov
%% v3.7, 7/19/17
%% input: TIFF image sequence of raw IR camera video, existing log file (for repeating settings from a previous analysis, optional), heat frame (for manually selecting the heating frame number, optional)
%% outputs: TIFF image sequences of Thermographic Signal Reconstruction (TSR), 1st derivative of TSR, and 2nd derivative of TSR, log file

%% uses least squares to fit a polynominal to the ln(temperature) vs. ln(time)
%% uses mldivide (\) to provide a least squares solution to A*x = B
%% x = A\B,  where x is the vector of least squares polynomial coefficients, A is the matrix of ln(time) values raised to each polynominal value, where B is ln(temperature),
%% requires copying tifflib.mexw64 from C:\Program Files\MATLAB\R2016a\toolbox\matlab\imagesci\private to current folder

%% start timer
tic

%% determine number of images in input TIFF sequence
info = imfinfo(filename);
numFiles = numel(info);

%% number of frames to include in the output TIFF sequence
output_frames = 200;

%% approximate number of frames to use to fit polynomial functions
approx_input_frames = 140;


mImage=info(1).Width;
nImage=info(1).Height;
% FileID = tifflib('open',filename,'r');
FileID = Tiff(filename,'r');
% rps = Tiff('getField',FileID,Tiff.TagID.RowsPerStrip);
rps = getTag(FileID,'RowsPerStrip');
Temp_Matrix_temp = zeros(nImage,mImage,'double');

%% determine settings based on whether a previous log file or heat frame is specified
switch nargin
    case 2
        [~,~,~,TSR_skip_frames] = textscan(existing_logfile,'%s %s %s %u',1, 'headerlines',4);
        [~,~,~,~,~,heat_event_frame] = textscan(existing_logfile,'%s %s %s %s %s %u\r\n',1, 'headerlines',5);
        [~,framerate] = textscan(existing_logfile,'%s %u',1, 'headerlines',6);
        [~,~,poly_degree] = textscan(existing_logfile,'%s %s %u',1, 'headerlines',7);
        [~,~,angle] = textscan(existing_logfile,'%s %s %u',1, 'headerlines',8);
        [xmin,ymin,pixel_width,pixel_height] = textscan(existing_logfile,'%u %u %u %u',1, 'headerlines',12);
    case 3
        [~,~,~,TSR_skip_frames] = textscan(existing_logfile,'%s %s %s %u',1, 'headerlines',4);
        [~,framerate] = textscan(existing_logfile,'%s %u',1, 'headerlines',6);
        [~,~,poly_degree] = textscan(existing_logfile,'%s %s %u',1, 'headerlines',7);
        [~,~,angle] = textscan(existing_logfile,'%s %s %u',1, 'headerlines',8);
        [xmin,ymin,pixel_width,pixel_height] = textscan(existing_logfile,'%u %u %u %u',1, 'headerlines',12);
    otherwise
        %% skip frames for TSR functionality, currently set to 0, consider doing more research on this number
        TSR_skip_frames = 0;

        %% polynominal degree (degree 6 selected based on literature and limited trial and error)
        poly_degree = 6;

        %% IR camera framerate (frames/second)
        framerate = 60;

        %% select region of interest by drawing a rectangle with the mouse, image displayed for rectangle selection is the middle frame of the video
        ROI_image = mat2gray(imread(filename, round(numFiles/2), 'Info', info));
        imshow(ROI_image)
        prompt = 'Rotate Image? (positive values = counter clockwise, 0 = no rotation): ';
        angle = input(prompt);
        angle2 = angle;
        while angle2 ~= 0
            close all
            imshow(imrotate(ROI_image,angle))
            angle2 = input(prompt);
            angle = angle + angle2;
        end
        fprintf('Use cursor to select Region of Interest \n')
        ROI = getrect;
        xmin = round(ROI(1));
        ymin = round(ROI(2));
        pixel_width = round(ROI(3));
        pixel_height = round(ROI(4));
        close all  %% closes figure
        
        
        
        %% find frame with the maximum temperature (consider changing to look only in the ROI)
        %% initialize maximum pixel value
        max_pixel = 0;
        
        %% loop to find frame with the maximum temperature value (consider swapping out if statement for find function)
        for i = 1:numFiles
            setDirectory(FileID,i);
            rps = min(rps,nImage);

            for r = 1:rps:nImage
                stripNum = computeStrip(FileID,r);
                current_strip = readEncodedStrip(FileID,stripNum);   %% save current image/frame
                max_pixel_current = max(current_strip(:));
                if max_pixel_current > max_pixel
                    heat_event_frame = i;
                    max_pixel = max_pixel_current;
                end
                
            end
        end
end

%% total number of frames to skip
% skip_frames = heat_event_frame-1 + TSR_skip_frames;
skip_frames = 0;

%% total remaining frames to analyze after subtracting out the skipped frames
frames_remaining = numFiles-skip_frames;

%% create an error if any negative pixel values exist (prevents natural log of pixel values creating complex values)
if(frames_remaining == 0)
    error('Heat Frame Selection Error (Last Frame is hottest)')
end

%% Create a 1D array of time data based on framerate
t = (1:frames_remaining)/framerate;

%% Take natural log (ln) of time
ln_t = log(t);

%% spacing between ln(time) inputs
delta_t = (ln_t(end)-ln_t(1))/approx_input_frames;

%% loop to select frames for fitting polynomial functions
%% initialize variables
index_vector = [];
running_diff = 0;

%% loop to find the indices for the selected frames
diff_ln_t = diff(ln_t);
for i = 1:frames_remaining-1
    if diff_ln_t(i) > delta_t
        index_vector = [index_vector; i];
    else
        running_diff = running_diff + diff_ln_t(i);
        if running_diff > delta_t
            index_vector = [index_vector; i];
            running_diff = 0;
        end
    end
end

%% actual number of input frames for fitting the polynomial functions
input_frames = length(index_vector);

%% remove ln(time) values that are not part of the input frames
ln_t = ln_t(index_vector);

%% initialize temperature matrix (to speed up script)
%% use double (rather than single) for temperature matrix to prevent a matrix rank error
Temp_Matrix = zeros(pixel_height, pixel_width, input_frames,'double');  %% matrix of all temperature pixel values for each input frame, after cropping, starting after the skipped frames


%% save grayscale values for each pixel for each image of TIFF sequence
if angle ~= 0
    for i = 1:input_frames
        setDirectory(FileID,skip_frames+index_vector(i)); %%check this!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        rps = min(rps,nImage);
        
        for r = 1:rps:nImage
            row_inds = r:min(nImage,r+rps-1);
            stripNum = computeStrip(FileID,r);
            Temp_Matrix_temp(row_inds,:) = double(readEncodedStrip(FileID,stripNum));   %% save current image/frame
        end
        Temp_Matrix_temp2 = imrotate(Temp_Matrix_temp,angle);  %% rotate image
        Temp_Matrix(:,:,i) = Temp_Matrix_temp2(ymin:ymin+pixel_height-1,xmin:xmin+pixel_width-1);  %% crop current image/frame and add it to the temperature matrix
    end
else
    for i = 1:input_frames
        %% save grayscale values and append data for image number "i" to a matrix for all images/frames
        %% use variable type "double" (convert from type uint16)
        setDirectory(FileID,index_vector(i));
        rps = min(rps,nImage);

        for r = 1:rps:nImage
            row_inds = r:min(nImage,r+rps-1);
            stripNum = computeStrip(FileID,r);
            Temp_Matrix_temp(row_inds,:) = double(readEncodedStrip(FileID,stripNum));   %% save current image/frame
        end
        Temp_Matrix(:,:,i) = Temp_Matrix_temp(ymin:ymin+pixel_height-1,xmin:xmin+pixel_width-1);  %% crop current image/frame and add it to the temperature matrix
    end
end


%% find 97% * min grayscale value for the last frame and subtract from each pixel for each frame
%% trial and error showed this worked well as a scaling value (subtracting less than the minimum prevents negative values and natural log errors, smaller values provide larger differences in natural log values)
min_Temp_t_end = min(min(Temp_Matrix(:,:,end)))*.97;
Temp_Matrix = Temp_Matrix - min_Temp_t_end;

%% create an error if any negative pixel values exist (prevents natural log of pixel values creating complex values)
if(min(Temp_Matrix(:))<=0)
    error('Negative Temperature Pixel Values Error')
end

%% Take natural log (ln) of temperature matrix
ln_Temp_Matrix = log(Temp_Matrix);


%% clear original time vector and temperature matrix (without ln) to save memory
clear t Temp_Matrix

%% create single type version of ln(t) for evaluating the output frames
sing_ln_t = single(linspace(min(ln_t),max(ln_t),output_frames));

%% initialize variables (to speed up script)
%% use singles to save space
ln_y = zeros(pixel_height, pixel_width, output_frames,'single');         %% matrix of the natural log of all temperature pixel values for each output frame
ln_y_1D = zeros(pixel_height, pixel_width, output_frames,'single');      %% matrix of the 1st derivative of a polynominal fit of the natural log of all temperature pixel values for each output frame

%% create vectors for operations prior to starting for loop
r = (poly_degree:-1:1)';
s = (poly_degree-1:-1:1)';
v = poly_degree:-1:0;
y = zeros(size(sing_ln_t),'single');

%% create matrix for polynomial fit
M = repmat(ln_t',1,poly_degree+1);
A = bsxfun(@power,M,v);

%% fit poly_degree order polynomial to ln_Temp_Matrix and calculate ln(T) values from polynomial
%% calculate 1st and 2nd derivatives
for j = 1:pixel_height
    for k = 1:pixel_width
        %% fit a poly_degree order polynomial to ln(T) vs. ln(t)
        %% x = A\B, where x is a vector of the polynomial contants and B is vector of ln(T) values
        x = single(A\(ln_Temp_Matrix(pixel_height*(k-1)+j:pixel_height*pixel_width:end))');
        
        %% take 1st and 2nd derivatives using the power rule
        x_1D = x(1:poly_degree).*r;
        x_2D = x_1D(1:poly_degree-1).*s;

        %% calculate ln(T) with new polynomial (using stripped down polyval function)
        y(:) = x(1);
        for a = 2:poly_degree+1
            y = sing_ln_t .* y + x(a);
        end
        ln_y(j,k,:) = y;

        %% calculate 1st derivative with new polynomial (using stripped down polyval function)
        y(:) = x_1D(1);
        for b = 2:poly_degree
            y = sing_ln_t .* y + x_1D(b);
        end
        ln_y_1D(j,k,:) = y;

        %% calculate 2nd derivative with new polynomial (using stripped down polyval function)
        y(:) = x_2D(1);
        for c = 2:poly_degree-1
            y = sing_ln_t .* y + x_2D(c);
        end
        ln_y_2D(j,k,:) = y;
    end
    
    %% displays progress based on what iteration of the loop it's on
%     display(['Analyzing Data: ',num2str(j*100/pixel_height,'%.0f'),'% Complete'])
end


%% clear matrices to save memory
clear M A ln_Temp_Matrix

%% save grayscale values for each pixel for every 15 seconds * 60 frames/sec of TIFF sequence
frame_spacing = 15*60;
counter = 1;
if angle ~= 0
    for i = skip_frames+1:frame_spacing:numFiles
        setDirectory(FileID,i);
        rps = min(rps,nImage);
        
        for r = 1:rps:nImage
            row_inds = r:min(nImage,r+rps-1);
            stripNum = computeStrip(FileID,r);
            Temp_Raw_Matrix_temp(row_inds,:) = double(readEncodedStrip(FileID,stripNum));   %% save current image/frame
        end
        Temp_Raw_Matrix_temp2 = imrotate(Temp_Raw_Matrix_temp,angle);  %% rotate image
        Temp_Raw_Matrix(:,:,counter) = Temp_Raw_Matrix_temp2(ymin:ymin+pixel_height-1,xmin:xmin+pixel_width-1);  %% crop current image/frame and add it to the temperature matrix
        counter = counter + 1;
    end
else
    for i = skip_frames+1:frame_spacing:numFiles
        %% save grayscale values and append data for image number "i" to a matrix for all images/frames
        %% use variable type "double" (convert from type uint16)
        setDirectory(FileID,i);
        rps = min(rps,nImage);

        for r = 1:rps:nImage
            row_inds = r:min(nImage,r+rps-1);
            stripNum = computeStrip(FileID,r);
            Temp_Raw_Matrix_temp(row_inds,:) = double(readEncodedStrip(FileID,stripNum));   %% save current image/frame
        end
        Temp_Raw_Matrix(:,:,counter) = Temp_Raw_Matrix_temp(ymin:ymin+pixel_height-1,xmin:xmin+pixel_width-1);  %% crop current image/frame and add it to the temperature matrix
        counter = counter + 1;
    end
end

for m = 1:output_frames
    %% convert matrices to 8 bit grayscale images, scales min value to 0 and max value to 255 (does not preserve temperature data)
    C = mat2gray(reshape(ln_y(:,:,m),[pixel_height, pixel_width]));
    D = mat2gray(reshape(ln_y_1D(:,:,m),[pixel_height, pixel_width]));
    E = mat2gray(reshape(ln_y_2D(:,:,m),[pixel_height, pixel_width]));
    
    %% save images as 8 bit grayscale TIFF sequences
    if m == 1
        imwrite(C,['Processed/',filename(1:end-5),' TSR.tiff'])
        imwrite(D,['Processed/',filename(1:end-5),' 1D.tiff'])
        imwrite(E,['Processed/',filename(1:end-5),' 2D.tiff'])
    else
        imwrite(C,['Processed/',filename(1:end-5),' TSR.tiff'],'WriteMode','append')
        imwrite(D,['Processed/',filename(1:end-5),' 1D.tiff'],'WriteMode','append')
        imwrite(E,['Processed/',filename(1:end-5),' 2D.tiff'],'WriteMode','append')
    end
    
    %% displays progress based on what iteration of the loop it's on
%     display(['Saving Output: ',num2str(m*100/output_frames,'%.0f'),'% Complete'])
end

for n = 1:size(Temp_Raw_Matrix,3)
    %% convert matrices to 8 bit grayscale images, scales min value to 0 and max value to 255 (does not preserve temperature data)
    F = mat2gray(Temp_Raw_Matrix(:,:,n));

    %% save images as 8 bit grayscale TIFF sequences
    if n == 1
        imwrite(F,['Processed/',filename(1:end-5),' Raw.tiff'])
    else
        imwrite(F,['Processed/',filename(1:end-5),' Raw.tiff'],'WriteMode','append')
    end
end


%% stop timer and display
total_time = toc;
fprintf(['Time Elapsed: ',num2str(total_time,'%.1f'),' sec'])

%% save log file
fileID = fopen(['Processed/',filename(1:end-5),' log.txt'],'w');
fprintf(fileID,'%s %s\r\n\r\n','Filename:',filename);
fprintf(fileID,'%s %s\r\n\r\n','Date/Time Processed:',datetime);
fprintf(fileID,'%s %u\r\n','TSR Skipped Frames:',TSR_skip_frames);
fprintf(fileID,'%s %u\r\n','Calculated Flash Bulb Frame Number:',min(heat_event_frame));
fprintf(fileID,'%s %u\r\n','Framerate:',framerate);
fprintf(fileID,'%s %u\r\n','Polynomial Degree:',poly_degree);
fprintf(fileID,'%s %.0f\r\n\r\n','Rotation (Degrees):',angle);
fprintf(fileID,'%s\r\n','Selected Region of Interest (pixels):');
fprintf(fileID,'%4s %6s %7s %8s\r\n','xmin','ymin','width','height');
fprintf(fileID,'%4u %6u %7u %8u\r\n\r\n',xmin,ymin,pixel_width,pixel_height);
fprintf(fileID,'%s %.1f %s','Elapsed Time:',total_time,'sec');
fclose(fileID);