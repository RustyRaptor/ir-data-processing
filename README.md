# IR Data Processing

## Scripts to process data from FLIR Boson cameras. 

### Prerequisites
- Bash (for running the capture script)
- Matlab
- OpenCV 3 or newer
- [BosonUSB](https://github.com/FLIR/BosonUSB)
- 

