filename = 'test4-8.raw';

fid = fopen(filename);
raw_data1 = fread(fid,'*uint16');

fid = fopen(filename);
fread(fid,1,'*uint8');
raw_data2 = fread(fid,'*uint16');

indices1 = find(raw_data1 == 33028)*2;  % multiply by 2 to convert to 8 bit
indices2 = find(raw_data2 == 33028)*2+1;   % multiply by 2 and add 1 to convert to 8 bit
indices = sort([indices1; indices2]);

clear raw_data1 raw_data2
fclose('all');

fid = fopen(filename);

for i = 1:length(indices)
    fseek(fid,indices(i)+3,'bof');
    tmp = fread(fid,[320,256],'*uint16');
    if i==1
        imwrite(tmp,['/home/ziad/Documents/BOSON/tiffs/',filename(1:end-4),'.tiff'])
    else
        imwrite(tmp,['/home/ziad/Documents/BOSON/tiffs/',filename(1:end-4),'.tiff'],'WriteMode','append')
    end
end

fclose(fid);